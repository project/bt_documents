<?php

namespace Drupal\bt_documents\Breadcrumb;

use Drupal\Core\Breadcrumb\Breadcrumb;
use Drupal\Core\Breadcrumb\BreadcrumbBuilderInterface;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\Core\Link;
use Drupal\Core\Config\ConfigFactory;

/**
 * Class DocumentsBreadcrumbBuilder.
 *
 * @package Drupal\bt_documents\Breadcrumb
 */
class DocumentsBreadcrumbBuilder implements BreadcrumbBuilderInterface {

  /**
   * The site name.
   *
   * @var string
   */
  protected $siteName;

  /**
   * The routes that will change their breadcrumbs.
   *
   * @var array
   */
  private $routes = [
    'entity.media.add_form',
    'entity.media.edit_form',
    'page_manager.page_view_app_website_documents_app_website_documents-panels_variant-0',
  ];

  /**
   * {@inheritdoc}
   */
  public function __construct(ConfigFactory $configFactory) {
    $this->siteName = $configFactory->get('system.site')->get('name');
  }

  /**
   * {@inheritdoc}
   */
  public function applies(RouteMatchInterface $route_match) {
    $match = $this->routes;
    if (in_array($route_match->getRouteName(), $match)) {
      if ($route_match->getRouteName() == 'entity.media.add_form') {
        if ($route_match->getParameters()->get('media_type')->get('id') == 'bt_public_document') {
          return TRUE;
        }
        else {
          return FALSE;
        }
      }
      elseif ($route_match->getRouteName() == 'entity.media.edit_form') {
        if ($route_match->getParameters()->get('media')->bundle() == 'bt_public_document') {
          return TRUE;
        }
        else {
          return FALSE;
        }
      }
      return TRUE;
    }
    else {
      return FALSE;
    }
  }

  /**
   * {@inheritdoc}
   */
  public function build(RouteMatchInterface $route_match) {
    $route = $route_match->getRouteName();
    $breadcrumb = new Breadcrumb();
    $breadcrumb->addCacheContexts(["url"]);

    $breadcrumb->addLink(Link::createFromRoute($this->siteName, 'page_manager.page_view_app_app-panels_variant-0'));
    $breadcrumb->addLink(Link::createFromRoute('Website', 'page_manager.page_view_app_website_app_website-panels_variant-0'));

    if ($route == 'entity.media.add_form' || $route == 'entity.media.edit_form') {
      $breadcrumb->addLink(Link::createFromRoute('Documents', 'page_manager.page_view_app_website_documents_app_website_documents-panels_variant-0'));
    }

    return $breadcrumb;
  }

}
