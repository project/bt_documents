<?php

namespace Drupal\bt_documents\Routing;

use Drupal\Core\Routing\RouteSubscriberBase;
use Symfony\Component\Routing\RouteCollection;

/**
 * Listens to the dynamic route events.
 */
class DocumentsRouteSubscriber extends RouteSubscriberBase {

  /**
   * {@inheritdoc}
   */
  public function alterRoutes(RouteCollection $collection) {
    // Exclude documents of media add list.
    if ($route = $collection->get('entity.media.add_page')) {
      $route->setDefault('_controller', '\Drupal\bt_documents\Controller\CustomDocumentsEntityController::addPage');
    }
  }

}
