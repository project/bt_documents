<?php

namespace Drupal\bt_documents\Controller;

use Drupal\bt_media\Controller\CustomMediaEntityController;

/**
 * Class CustomDocumentsEntityController.
 *
 * @package Drupal\bt_documents\Controller
 */
class CustomDocumentsEntityController extends CustomMediaEntityController {

  /**
   * {@inheritdoc}
   */
  public function addPage($entity_type_id) {
    $build = parent::addPage($entity_type_id);
    unset($build['#bundles']['bt_public_document']);

    return $build;
  }

}
